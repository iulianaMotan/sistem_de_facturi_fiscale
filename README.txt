Brinzoi Iuliana 323CC
Tema a fost de dificultate medie.
Timp alocat: 4 zile

Task 1
Pentru a realiza task-ul 1 am creat clasele specificate in enunt,
precum si alte clase ajutatoare, cu care creez diverse obiecte, necesare
popularii obiectului gestiune.
Prin parsarea fisierelor date am creat vectorul de produse, de facturi si de magazine,
adaugand acesti vectori obiectului Gestiune. Prin prelucrarea obiectului Gestiune
am creat fisierul out.txt, necesar finalizarii task-ului 1.

Task 2
In cadrul interfetei grafice, am creat fereastra principala care contine 3 butoane,
aferente celor 3 ferestre specificate in aunt. Fiecare ferestra contine o lista
cu optiunile necesare fiecareia. Folosindu-ma de obiectul gestiune si de parsarea
fisierelor de intrare, aplicatia modisica sau afiseaza produsele si statisticile
necesare.