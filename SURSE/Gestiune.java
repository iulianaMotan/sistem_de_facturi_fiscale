import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Gestiune {
	
	private static Gestiune gestiune = new Gestiune();

	ArrayList <Produs> produse = new ArrayList<>();
	ArrayList <Magazin> magazine = new ArrayList<>();
	Map<String, HashMap<String, Double>> taxe = new HashMap<String, HashMap<String, Double>>(); 
	
	public static Gestiune getInstance() {
		return gestiune;
	}
}
