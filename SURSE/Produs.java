
public class Produs {
	
	private String denumire;
	private String categorie;
	private String taraOrigine;
	private double pret;
	
	public Produs(String denumire, String categorie, String taraOrigine, double pret) {
		this.denumire = new String(denumire);
		this.categorie = new String(categorie);
		this.taraOrigine = new String(taraOrigine);
		this.pret = pret;
	}
	
	public Produs() {
		this(null, null, null, 0);
	}
	
	void setDenumire(String denumire) {
		this.denumire = new String(denumire);
	}
	
	String getDenumire() {
		return this.denumire;
	}
	
	void setCategorie(String categorie) {
		this.categorie = new String(categorie);
	}
	
	String getCategorie() {
		return this.categorie;
	}
	
	void setTaraOrigine(String taraOrigine) {
		this.taraOrigine = new String(taraOrigine);
	}
	
	String getTaraOrigine() {
		return this.taraOrigine;
	}
	
	void setPret(double pret) {
		this.pret = pret;
	}
	
	double getPret() {
		return this.pret;
	}
	
	public String toString() {
		return "Produs: " + denumire + " " + categorie + " " + taraOrigine + " " + pret;
	}

}
