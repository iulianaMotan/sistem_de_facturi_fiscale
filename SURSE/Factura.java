import java.util.Iterator;
import java.util.Vector;

public class Factura {
	
	String denumire;
	Vector <ProdusComandat> produseComandate = new Vector <>();
	
	public Factura(String denumire, Vector <ProdusComandat> produseComandate) {
		this.denumire = new String(denumire);
		Iterator <ProdusComandat> iter = produseComandate.iterator();
		while(iter.hasNext())
			this.produseComandate.add(iter.next());
	}
	
	public Factura() {
		this(null, null);
	}
	
	double getTotalFaraTaxe() {
		Iterator <ProdusComandat> iter = produseComandate.iterator();
		double total = 0;
		ProdusComandat prod;
		while(iter.hasNext()) {
			prod = iter.next();
			total  = total + prod.getProdus().getPret() * prod.getCantitate();
		}
		
		return total;
	}
	
	
	double getTotalCuTaxe() {
		Iterator <ProdusComandat> iter = produseComandate.iterator();
		double total = 0, taxa;
		ProdusComandat prod;
		while(iter.hasNext()) {
			prod = iter.next();
			taxa = 1 + prod.getTaxa() / 100;
			total  = total + prod.getProdus().getPret() * taxa * prod.getCantitate();
		}
		
		return total;
	}
	
	double getTaxe() {
		Iterator <ProdusComandat> iter = produseComandate.iterator();
		double total = 0, taxa;
		ProdusComandat prod;
		while(iter.hasNext()) {
			prod = iter.next();
			taxa = prod.getProdus().getPret() * prod.getTaxa() / 100;
			total  = total + taxa;
		}
		return total;
	}
	
	double getTotalTaraFaraTaxe(String tara) {
		Iterator <ProdusComandat> iter = produseComandate.iterator();
		double total = 0;
		ProdusComandat prod;
		while(iter.hasNext()) {
			prod = iter.next();
			if(prod.getProdus().getTaraOrigine().equals(tara))
				total  = total + prod.getProdus().getPret() * prod.getCantitate();
		}
		return total;
	}
	
	double getTotalTaraCuTaxe(String tara) {
		Iterator <ProdusComandat> iter = produseComandate.iterator(); 
		double total = 0, taxa;
		ProdusComandat prod;
		while(iter.hasNext()) {
			prod = iter.next();
			if(prod.getProdus().getTaraOrigine().equals(tara)) {
				taxa = 1 + prod.getTaxa() / 100;
				total  = total + prod.getProdus().getPret() * taxa * prod.getCantitate();
			}
		}
		return total;
	}
	
	double getTaxeTara(String tara) {
		Iterator <ProdusComandat> iter = produseComandate.iterator();
		double total = 0, taxa;
		ProdusComandat prod;
		while(iter.hasNext()) {
			prod = iter.next();
			taxa = prod.getProdus().getPret() * prod.getTaxa() / 100;
			total  = total + taxa;
		}
		return total;
	}
	
	public String toString() {
		return denumire + " " + produseComandate;
	}

}
