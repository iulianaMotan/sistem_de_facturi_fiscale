import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

public abstract class Magazin implements IMagazin {
	
	String nume;
	Vector <Factura> factura = new Vector<>();
	private TipMagazin tip = null;
	
	public Magazin(TipMagazin tip, String nume, Vector <Factura> factura) {
		this.tip = tip;
		this.nume = new String(nume);
		Iterator <Factura> iter = factura.iterator();
		while(iter.hasNext())
			this.factura.add(iter.next());
	}
	
	public void setTip(TipMagazin tip) {
		this.tip = tip;
	}
	
	public TipMagazin getTip() {
		return this.tip;
	}
	
	public double getTotalFaraTaxe() {
		Iterator <Factura> iter = factura.iterator();
		double total = 0;
		Factura fact;
		while(iter.hasNext()) {
			fact = iter.next();
			total = total + fact.getTotalFaraTaxe();
		}
		
		return total;
		
	}
	
	public double getTotalCuTaxe() {
		Iterator <Factura> iter = factura.iterator();
		double total = 0;
		Factura fact;
		while(iter.hasNext()) {
			fact = iter.next();
			total = total + fact.getTotalCuTaxe();
		}
		
		return total;
	}
	public double getTotalCuTaxeScutite() {
		RandomAccessFile file = null;
		String str, tari[] = null, vect[] = null;
		HashMap <String, Double> categorie =  new HashMap<>();
		Double total, taxa;
		String cat;
		try {
			file = new RandomAccessFile("src/produse.txt", "r");
			str = file.readLine();
			tari = str.split(" ");	
			while((str = file.readLine()) != null) {
				vect = str.split(" ");
				categorie.put(vect[1], 0.0);
			}
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
		finally {
			if(file != null) {
				try {
					file.close();
				}
				catch(IOException e){
					e.printStackTrace();
				}
			}
		}
		switch(tip) {
			case MiniMarket:
				for(int i = 2; i < tari.length; i++) {
					if(getTotalTaraCuTaxe(tari[i]) > 0.5 * getTotalCuTaxe())
						return getTotalCuTaxe() - 0.1 * getTotalCuTaxe();
				}
				break;
			case MediumMarket:
				Iterator <Factura> iterFactura = factura.iterator();
				Iterator <ProdusComandat> iterProdus;
				ProdusComandat prod;
				while(iterFactura.hasNext()) {
					iterProdus = iterFactura.next().produseComandate.iterator();
					while(iterProdus.hasNext()) {
						prod = iterProdus.next();
						cat = new String(prod.getProdus().getCategorie());
						taxa = prod.getProdus().getPret() * (1 + prod.getTaxa() / 100) * prod.getCantitate();
						total = categorie.get(cat);
						categorie.put(cat, total + taxa);
						}
					}
					Iterator <Double> iter = categorie.values().iterator();
					while(iter.hasNext()) {
						if(iter.next() > 0.5 * getTotalCuTaxe())
							return getTotalCuTaxe() - 0.05 * getTotalCuTaxe();
					}
				break;
			case HyperMarket:
				Iterator <Factura> iterF = factura.iterator();
				while(iterF.hasNext()) {
					if(iterF.next().getTotalCuTaxe() > 0.1 * getTotalCuTaxe())
						return getTotalCuTaxe() - 0.01 * getTotalCuTaxe();
				}
				break;
			}
		return getTotalCuTaxe();
	}
	public double getTotalTaraFaraTaxe(String tara) {
		Iterator <Factura> iter = factura.iterator();
		Iterator <ProdusComandat> iterProd;
		ProdusComandat prod;
		double total = 0;
		Factura fact;
		while(iter.hasNext()) {
			fact = iter.next();
			iterProd = fact.produseComandate.iterator();
			while(iterProd.hasNext()) {
				prod = iterProd.next();
				if(prod.getProdus().getTaraOrigine().equals(tara))
					total = total + prod.getProdus().getPret() * prod.getCantitate();;
			}
		}
		
		return total;
	}
	public double getTotalTaraCuTaxe(String tara) {
		Iterator <Factura> iter = factura.iterator();
		Iterator <ProdusComandat> iterProd;
		ProdusComandat prod;
		double total = 0, taxa;
		Factura fact;
		while(iter.hasNext()) {
			fact = iter.next();
			iterProd = fact.produseComandate.iterator();
			while(iterProd.hasNext()) {
				prod = iterProd.next();
				if(prod.getProdus().getTaraOrigine().equals(tara)) {
					taxa = 1 + prod.getTaxa() / 100;
					total  = total + prod.getProdus().getPret() * taxa * prod.getCantitate();
				}
			}
		}
		return total;
	}
	public double getTotalTaraCuTaxeScutite(String tara) {
		RandomAccessFile file = null;
		String str, tari[] = null, vect[] = null;
		HashMap <String, Double> categorie =  new HashMap<>();
		Double total, taxa;
		String cat;
		try {
			file = new RandomAccessFile("src/produse.txt", "r");
			str = file.readLine();
			tari = str.split(" ");	
			while((str = file.readLine()) != null) {
				vect = str.split(" ");
				categorie.put(vect[1], 0.0);
			}
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
		finally {
			if(file != null) {
				try {
					file.close();
				}
				catch(IOException e){
					e.printStackTrace();
				}
			}
		}
		switch(tip) {
			case MiniMarket:
				for(int i = 2; i < tari.length; i++) {
					if(getTotalTaraCuTaxe(tari[i]) > 0.5 * getTotalCuTaxe())
						return getTotalTaraCuTaxe(tara) - 0.1 * getTotalTaraCuTaxe(tara);
				}
				break;
			case MediumMarket:
				Iterator <Factura> iterFactura = factura.iterator();
				Iterator <ProdusComandat> iterProdus;
				ProdusComandat prod;
				while(iterFactura.hasNext()) {
					iterProdus = iterFactura.next().produseComandate.iterator();
					while(iterProdus.hasNext()) {
						prod = iterProdus.next();
						cat = new String(prod.getProdus().getCategorie());
						taxa = prod.getProdus().getPret() * (1 + prod.getTaxa() / 100) * prod.getCantitate();
						total = categorie.get(cat);
						categorie.put(cat, total + taxa);
						}
					}
					Iterator <Double> iter = categorie.values().iterator();
					while(iter.hasNext()) {
						if(iter.next() > 0.5 * getTotalCuTaxe())
							return getTotalTaraCuTaxe(tara) - 0.05 * getTotalTaraCuTaxe(tara);
					}
				break;
			case HyperMarket:
				Iterator <Factura> iterF = factura.iterator();
				while(iterF.hasNext()) {
					if(iterF.next().getTotalCuTaxe() > 0.1 * getTotalCuTaxe())
						return getTotalTaraCuTaxe(tara) - 0.01 * getTotalTaraCuTaxe(tara);
				}
				break;
			}
		return getTotalTaraCuTaxe(tara);	
	}
	
	public abstract double calculScutiriTaxe();
	public String toString() {
		String str = nume + " ";
		Iterator <Factura> iter = factura.iterator();
		while(iter.hasNext()) {
			str = str + iter.next().toString() + "\n";
		}
		return str;
	}
}
