import javax.swing.*;
import javax.swing.border.TitledBorder;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Vector;

public class FereastraStart extends JFrame implements ActionListener, KeyListener{
	
	final JPanel panel = new JPanel();
	JList <String> scroll, optionScroll;
	JScrollPane listScroller, optionScroller;
	int actionFlag = 0;
	static int obiectCreat = 0;
	FileReader file = null;
	BufferedReader br = null;
	JTextArea textArea;
	Gestiune gestiune = Gestiune.getInstance();
	CreeazaProdus creeazaProdus;
	FereastraInterna f1 = null, f2 = null, f3 = null, f4 = null;
	public FereastraStart (String titlu) {
		super (titlu);
		setSize (500, 500) ;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE );
	}
	
	void creeazaFereastra() {
		
		panel.setPreferredSize(new Dimension (500 ,500));
		panel.setBackground(new java.awt.Color(0, 18, 50));
        panel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        
		TitledBorder title;
		title = BorderFactory.createTitledBorder (" Start ");
		title.setTitleColor(new Color(240, 240, 240));
		panel.setBorder(title);
		getContentPane().add(panel);
		
		JButton b1 = new JButton("Gestiune fisiere");
		JButton b2 = new JButton("Administare produse");
		JButton b3 = new JButton("Afisare statistici");
		b1.addActionListener(this);
		b2.addActionListener(this);
		b3.addActionListener(this);
		b1.setActionCommand("Gestiune fisiere");
		b2.setActionCommand("Administrare produse");
		b3.setActionCommand("Afisare statistici");
		
		b1.setBackground(new java.awt.Color(32, 47, 90));
        b2.setBackground(new java.awt.Color(32, 47, 90));
        b3.setBackground(new java.awt.Color(32, 47, 90));
        
		panel.add(b1);
		panel.add(b2);
		panel.add(b3);
		
	}
	
	public void FereastraGestiuneFisiere() {
		actionFlag = 1;
		f1 = new FereastraInterna("Gestiune");
		f1.setVisible(true);
		f1.setPreferredSize(new Dimension(170, 170));
		Vector <String> vect = new Vector<>();
		vect.add(new String("Creeaza Gestiune"));
		vect.add(new String("Creeaza out.txt"));
		scroll = new JList<>(vect);
		scroll.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		scroll.setLayoutOrientation(JList.HORIZONTAL_WRAP);
		scroll.setVisibleRowCount(-1);
		scroll.setFocusable(true);
		scroll.addKeyListener(this);
		listScroller = new JScrollPane(scroll);
		listScroller.setPreferredSize(new Dimension(120, 100));
		f1.add(listScroller);
		panel.add(f1);
	}
	
	public void FereastraAdministrareProduse() {
		actionFlag = 2;
		f2 = new FereastraInterna("Administrare Produse");
		f2.setVisible(true);
		f2.setPreferredSize(new Dimension(170, 170));
		Vector <String> vect = new Vector<>();
		vect.add(new String("Afiseaza Produse"));
		vect.add(new String("Adaugare Produs"));
		vect.add(new String("Stergere Produs"));
		scroll = new JList<>(vect);
		scroll.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		scroll.setLayoutOrientation(JList.HORIZONTAL_WRAP);
		scroll.setVisibleRowCount(-1);
		scroll.setFocusable(true);
		scroll.addKeyListener(this);
		listScroller = new JScrollPane(scroll);
		listScroller.setPreferredSize(new Dimension(120, 100));
		f2.add(listScroller);
		panel.add(f2);
	}
	
	public void FereastraAfisareStatistici() {
		actionFlag = 3;
		f3 = new FereastraInterna("Afisare statistici");
		f3.setVisible(true);
		f3.setPreferredSize(new Dimension(280, 170));
		f3.setFocusTraversalKeysEnabled(true);
		Vector <String> vect = new Vector<>();
		vect.add(new String("Cele mai mari vanzari"));
		vect.add(new String("Cele mai mari vanzari pentru fiecare tara"));
		vect.add(new String("Cele mai mari vanzari pentru fiecare categorie"));
		vect.add(new String("Factura cea mai mare"));
		scroll = new JList<>(vect);
		scroll.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		scroll.setLayoutOrientation(JList.HORIZONTAL_WRAP);
		scroll.setVisibleRowCount(-1);
		scroll.setFocusable(true);
		scroll.addKeyListener(this);
		listScroller = new JScrollPane(scroll);
		listScroller.setPreferredSize(new Dimension(120, 100));
		f3.add(listScroller);
		panel.add(f3);
	}
	
	public File alegeFisier() {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
		int result = fileChooser.showOpenDialog(this);
		if (result == JFileChooser.APPROVE_OPTION) {
		    File selectedFile = fileChooser.getSelectedFile();
		    return selectedFile;
		}
		return null;
	}
	
	public void modificaFisier(File inputFile, String name) {
		File outputFile = new File(name);
		BufferedReader readFile = null;
		BufferedWriter writeFile = null;
		String str;
		try {
			readFile = new BufferedReader(new FileReader(inputFile));
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		ArrayList <String> toWrite = new ArrayList<>();
		try {
			while((str = readFile.readLine()) != null) {
				toWrite.add(str);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			if(readFile != null) {
				try {
					readFile.close();
				}
				catch(IOException e){
					e.printStackTrace();
				}
			}
		}
		try {
			writeFile = new BufferedWriter(new FileWriter(outputFile));
			for(int i = 0; i < toWrite.size(); i++) {
				writeFile.write(toWrite.get(i));
				if(toWrite.size() - i != 1)
					writeFile.newLine();
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		finally {
			if(writeFile != null) {
				try {
					writeFile.close();
				}
				catch(IOException e){
					e.printStackTrace();
				}
			}
		}
	}

	public void actionPerformed(ActionEvent e) {
		switch(e.getActionCommand()) {
			case "Gestiune fisiere":
				if((f2 == null || f2.isClosed()) && (f3 == null || f3.isClosed())) {
					FereastraGestiuneFisiere();
					break;
				}
				else {
					if(f3 != null)
						if(!f3.isClosed())
							f3.doDefaultCloseAction();
					if(f2 != null)
						if(!f2.isClosed())
							f2.doDefaultCloseAction();
					FereastraAfisareStatistici();
				}
				break;
			case "Administrare produse":
				if((f1 == null || f1.isClosed()) && (f3 == null || f3.isClosed())) {
					FereastraAdministrareProduse();
					break;
				}
				else {
					if(f1 != null)
						if(!f1.isClosed())
							f1.doDefaultCloseAction();
					if(f3 != null)
						if(!f3.isClosed())
							f3.doDefaultCloseAction();
					FereastraAdministrareProduse();
				}
				break;
			case "Afisare statistici":
				if((f1 == null || f1.isClosed()) && (f2 == null || f2.isClosed())) {
					FereastraAfisareStatistici();
					break;
				}
				else {
					if(f1 != null)
						if(!f1.isClosed())
							f1.doDefaultCloseAction();
					if(f2 != null)
						if(!f2.isClosed())
							f2.doDefaultCloseAction();
					FereastraAfisareStatistici();
				}
				break;
		}
	}

	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_ENTER) {
			valueChanged(e);
		}
	}
	public void keyReleased(KeyEvent e) {}
	public void keyTyped(KeyEvent e) {}
	
	public void valueChanged(KeyEvent e) {
	    if (scroll.isSelectionEmpty() == false) {
		    	if(scroll.getSelectedIndex() == 0 && (f4 == null || f4.isClosed())) {
		    		switch(actionFlag) {
		    			case 1:
		    				JOptionPane.showMessageDialog(null, "Alegeti fisierul produse.txt");
		    				File produs = alegeFisier();
		    				if(produs == null) {
		    					JOptionPane.showMessageDialog(null, "Obiectul Gestiune nu a fost creat!");
		    					break;
		    				}
		    				modificaFisier(produs, "src/produse.txt");
		    				JOptionPane.showMessageDialog(null, "Alegeti fisierul taxe.txt");
		    				File taxe = alegeFisier();
		    				if(taxe == null) {
		    					JOptionPane.showMessageDialog(null, "Obiectul Gestiune nu a fost creat!");
		    					break;
		    				}
		    				modificaFisier(taxe, "src/taxe.txt");
		    				JOptionPane.showMessageDialog(null, "Alegeti fisierul facturi.txt");
		    				File facturi = alegeFisier();
		    				if(facturi == null) {
		    					JOptionPane.showMessageDialog(null, "Obiectul Gestiune nu a fost creat!");
		    					break;
		    				}
		    				modificaFisier(facturi, "src/facturi.txt");
		    				CreeazaGestiune obiectGestiune = new CreeazaGestiune();
		    				obiectGestiune.creeazaGestiune();
		    				obiectCreat = 1;
		    				JOptionPane.showMessageDialog(null, "Obiect Gestiune creat cu succes!");
		    				break;
		    			case 2:
		    				creeazaProdus = new CreeazaProdus();
		    				creeazaProdus.creeaza();
		    				f4 = new FereastraInterna("Ordonare produse");
		    				f4.setVisible(true);
		    				f4.setPreferredSize(new Dimension(130, 100));
		    				Vector <String> vect = new Vector<>();
		    				vect.add("Neordonate");
		    				vect.add("Ordonate alfabetic");
		    				vect.add("Ordonate dupa tara");
		    				optionScroll = new JList<>(vect);
		    				optionScroll.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		    				optionScroll.setLayoutOrientation(JList.HORIZONTAL_WRAP);
		    				optionScroll.setVisibleRowCount(-1);
		    				optionScroll.setFocusable(true);
		    				optionScroll.addKeyListener(this);
		    				optionScroller = new JScrollPane(optionScroll);
		    				optionScroller.setPreferredSize(new Dimension(120, 100));
		    				f4.add(optionScroller);
		    				panel.add(f4);
		    				break;
		    			case 3:
		    				if(obiectCreat == 0) {
		    					JOptionPane.showMessageDialog(null, "Obiectul Gestiune nu a fost creat inca!");
		    					break;
		    				}
		    				else {
		    					Statistici statistici = new Statistici();
		    					JOptionPane.showMessageDialog(null, statistici.celeMaiMariVanzari());
		    				}
		    				break;
		    		}
		    	}
		    	if(scroll.getSelectedIndex() == 1) {
		    		switch(actionFlag) {
		    			case 1:
		    				if(obiectCreat == 0)
		    					JOptionPane.showMessageDialog(null, "Eroare, obiectul Gestiune nu a fost creat inca!");
		    				else {
		    					CreeazaOut obiectOut = new CreeazaOut();
		    					obiectOut.creeazaOut();
		    					JOptionPane.showMessageDialog(null, "Fisier \"out.txt\" creat cu succes!");
		    				}
		    				break;
		    			case 2:
		    				creeazaProdus = new CreeazaProdus();
		    				creeazaProdus.creeaza();
		    				String denumire = null;
	    					String categorie = null;
	    					ArrayList <String> pret = new ArrayList<>();
	    					denumire = new String(JOptionPane.showInputDialog("Denumire"));
	    					categorie = new String(JOptionPane.showInputDialog("Categorie"));
	    					for(int i = 2; i < creeazaProdus.tari.length; i++) {
	    						pret.add(JOptionPane.showInputDialog("Pret " + creeazaProdus.tari[i]));
	    					}
	    					ModificareProdus obiectProdus = new ModificareProdus();
	    					int rezultat = obiectProdus.adaugareProdus(denumire, categorie, pret);
	    					if(rezultat == -1)
	    						JOptionPane.showMessageDialog(null, "Produsul exista deja!");
	    					else 
	    						JOptionPane.showMessageDialog(null, "Produs adaugat cu success!");
		    				break;
		    			case 3:
		    				if(obiectCreat == 0) {
		    					JOptionPane.showMessageDialog(null, "Obiectul Gestiune nu a fost creat inca!");
		    					break;
		    				}
		    				else{
		    					Statistici statistici = new Statistici();
		    					JOptionPane.showMessageDialog(null, statistici.celeMaiMariVanzariTara());
		    				}
		    				break;
		    		}
		    	}
		    	if(scroll.getSelectedIndex() == 2) {
		    		switch(actionFlag) {
		    			case 2:
		    				String denumire = null;
		    				String categorie = null;
		    				denumire = new String(JOptionPane.showInputDialog("Denumire"));
	    					categorie = new String(JOptionPane.showInputDialog("Categorie"));
		    				ModificareProdus obiectProdus = new ModificareProdus();
	    					int rezultat = obiectProdus.stergereProdus(denumire, categorie);
	    					if(rezultat == -1)
	    						JOptionPane.showMessageDialog(null, "Produsul nu exista");
	    					else 
	    						JOptionPane.showMessageDialog(null, "Produs sters cu success!");
		    				break;
		    			case 3:
		    				if(obiectCreat == 0) {
		    					JOptionPane.showMessageDialog(null, "Obiectul Gestiune nu a fost creat inca!");
		    					break;
		    				}
		    				else {
		    					Statistici statistici = new Statistici();
		    					JOptionPane.showMessageDialog(null, statistici.celeMaiMariVanzariCategorie());
		    				}
		    				break;
		    		}
		    	}
		    	if(scroll.getSelectedIndex() == 3) {
		    		if(obiectCreat == 1) {
		    			Statistici statistici = new Statistici();
	    				JOptionPane.showMessageDialog(null, statistici.ceaMaiMareFactura());
    				}
		    		else {
		    			JOptionPane.showMessageDialog(null, "Obiectul Gestiune nu a fost creat inca!");
		    		}
		    	}
		    	if(f4 != null) {
			    	if (optionScroll.isSelectionEmpty() == false && !f4.isClosed() ) {
				    	if(optionScroll.getSelectedIndex() == 0) {
				    		JOptionPane.showMessageDialog(null, creeazaProdus.creeazaString());
				    		f4.doDefaultCloseAction();
				    	}
				    	else if(optionScroll.getSelectedIndex() == 1) {
				    		creeazaProdus.ordoneazaAlfabetic();
				    		JOptionPane.showMessageDialog(null, creeazaProdus.creeazaString());
				    		f4.doDefaultCloseAction();
				    	}
				    	else if(optionScroll.getSelectedIndex() == 2) {
				    		creeazaProdus.ordoneazaDupaTara();
				    		JOptionPane.showMessageDialog(null, creeazaProdus.creeazaString());
				    		f4.doDefaultCloseAction();
				    	}
				    }
		    	}
	    }
	}	
}