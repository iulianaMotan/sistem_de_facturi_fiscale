import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

public class MediumMarket extends Magazin{

	public MediumMarket(String nume, Vector<Factura> factura) {
		super(TipMagazin.MediumMarket, nume, factura);
	}

	public double calculScutiriTaxe() {
		Iterator <Factura> iterFactura = factura.iterator();
		Iterator <ProdusComandat> iterProdus;
		ProdusComandat prod = new ProdusComandat();
		HashMap <String, Double> categorie = new HashMap<>();
		Double total, taxa;
		String cat;
		while(iterFactura.hasNext()) {
			iterProdus = iterFactura.next().produseComandate.iterator();
			while(iterProdus.hasNext()) {
				prod = iterProdus.next();
				cat = new String(prod.getProdus().getCategorie());
				taxa = prod.getProdus().getPret() * (1 + iterProdus.next().getTaxa() / 100);
				if(!categorie.containsKey(cat))
					categorie.put(cat, taxa);
				else {
				total = categorie.get(cat);
				categorie.put(cat, total + taxa);
				}
			}
			Iterator <Double> iter = categorie.values().iterator();
			while(iter.hasNext()) {
				if(iter.next() > 0.5 * getTotalCuTaxe())
					return 5;
			}
		}
		return 0;
	}

}
