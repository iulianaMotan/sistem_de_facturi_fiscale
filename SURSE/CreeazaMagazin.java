import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Vector;

public class CreeazaMagazin {
	Gestiune gestiune = Gestiune.getInstance();
	CreeazaFactura obiectFactura = new CreeazaFactura();
	RandomAccessFile file = null;
	String str, nume, tip;
	String vect[];
	int contor = 0;
	Vector <Factura> deAdaugat;
	Magazin magazin;
	void creeazaMagazin() {
		try {
			obiectFactura.creeazaFactura();
			file = new RandomAccessFile("src/facturi.txt", "r");
			str = file.readLine();
			do {
				if(str.startsWith("Magazin")) {
					vect = str.split(":");
					nume = new String(vect[2]);
					tip = new String(vect[1]);
					str = file.readLine();
					while(!str.startsWith("Magazin")) {
						if(str.startsWith("Factura"))
							contor++;
						str = file.readLine();
						if(str == null)
							break;
					}
					deAdaugat = new Vector<>();
					for(int i = 0; i < contor; i++) 
						deAdaugat.add(obiectFactura.facturi.get(i));
					switch(tip) {
						case "MiniMarket":
							magazin = new MiniMarket(nume, deAdaugat);
							break;
						case "MediumMarket":
							magazin = new MediumMarket(nume, deAdaugat);
							break;
						case "HyperMarket":
							magazin = new HyperMarket(nume, deAdaugat);
							break;
					}
					gestiune.magazine.add(magazin);
					obiectFactura.facturi.removeAll(deAdaugat);
					contor = 0;					
				}
			}
			while(str != null);
				
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
		finally {
			if(file != null) {
				try {
					file.close();
				}
				catch(IOException e){
					e.printStackTrace();
				}
			}
		}
	}
}
