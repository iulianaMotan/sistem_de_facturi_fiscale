import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;

public class CompleteazaGestiune {
	Gestiune gestiune = Gestiune.getInstance();
	RandomAccessFile file = null;
	String str;
	String[] tari, vect;
	ArrayList<String[]> matrice = new ArrayList<>();
	GetHashMap pereche;
	void citeste() {
		try {
			file = new RandomAccessFile("src/taxe.txt", "r");
			str = file.readLine();
			tari = str.split(" ");
			str = file.readLine();
			do {
				vect = str.split(" ");
				matrice.add(vect);
				str = file.readLine();
			}
			while(str != null);
				
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
		finally {
			if(file != null) {
				try {
					file.close();
				}
				catch(IOException e){
					e.printStackTrace();
				}
			}
		}
	}
	void completeaza() {
		for(int i = 1; i < tari.length; i++) {
			pereche = new GetHashMap();
			for(int j = 0; j < matrice.size(); j++) {
				pereche.addEntry(matrice.get(j)[0], Double.parseDouble(matrice.get(j)[i]));
			}
			gestiune.taxe.put(tari[i], pereche.getInstanceOfHashMap());
		}
	}
}
