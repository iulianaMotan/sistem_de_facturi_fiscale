import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Vector;

public class MiniMarket extends Magazin{

	public MiniMarket(String nume, Vector<Factura> factura) {
		super(TipMagazin.MiniMarket, nume, factura);
	}

	public double calculScutiriTaxe() {
		RandomAccessFile file = null;
		String str, tari[] = null;
		try {
			file = new RandomAccessFile("src/produse.txt", "r");
			str = file.readLine();
			tari = str.split(" ");	
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
		finally {
			if(file != null) {
				try {
					file.close();
				}
				catch(IOException e){
					e.printStackTrace();
				}
			}
		}
		for(int i = 1; i < tari.length; i++)
			if(getTotalTaraCuTaxe(tari[1]) > 0.5 * getTotalCuTaxe())
				return 10;
		return 0;
	}

}
