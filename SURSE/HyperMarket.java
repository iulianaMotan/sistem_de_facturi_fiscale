import java.util.Iterator;
import java.util.Vector;

public class HyperMarket extends Magazin{

	public HyperMarket(String nume, Vector<Factura> factura) {
		super(TipMagazin.HyperMarket, nume, factura);
	}
	
	public double calculScutiriTaxe() {
		Iterator <Factura> iter = factura.iterator();
		while(iter.hasNext()) {
			if(iter.next().getTotalCuTaxe() > 0.1 * getTotalCuTaxe())
				return 1;
		}
		return 0;
	}
}
