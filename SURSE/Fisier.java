import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class Fisier {
	
	void scrie() {
		RandomAccessFile fisier = null;
		try {
			fisier = new RandomAccessFile("src/out.txt", "rw");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		finally{
			try {
				fisier.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
