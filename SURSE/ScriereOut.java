import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Vector;

public class ScriereOut {
	RandomAccessFile readFile = null;
	File fout = new File("src/out.txt");
	FileOutputStream fos = null;
	BufferedWriter writeFile = null;
	
	Gestiune gestiune = Gestiune.getInstance();
	ArrayList<Magazin> miniMarket = new ArrayList<>();
	ArrayList <Magazin> mediumMarket = new ArrayList<>();
	ArrayList <Magazin> hyperMarket = new ArrayList<>();
	
	String[] citireTari(){
		String str = null;
		try {
			readFile = new RandomAccessFile("src/taxe.txt", "r");
			str = readFile.readLine();
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
		finally {
			if(readFile != null) {
				try {
					readFile.close();
				}
				catch(IOException e){
					e.printStackTrace();
				}
			}
		}
		return str.split(" ");
	}
	
	void atribuireMagazine(){
		Iterator <Magazin> iter = gestiune.magazine.iterator();
		Magazin magazin;
		TipMagazin tip;
		while(iter.hasNext()) {
			magazin = iter.next();
			tip = magazin.getTip();
			switch(tip) {
				case MiniMarket:
					miniMarket.add((MiniMarket)magazin);
					break;
				case MediumMarket:
					mediumMarket.add((MediumMarket)magazin);
					break;
				case HyperMarket:
					hyperMarket.add((HyperMarket)magazin);
					break;
			}
		}
	}
	
	void sortareMagazine(ArrayList <Magazin> magazin) {
		Collections.sort(magazin, new Comparator<Magazin>(){
			public int compare(Magazin o1, Magazin o2) {
				if(o1.getTotalFaraTaxe() > o2.getTotalFaraTaxe())
					return 1;
				if(o1.getTotalFaraTaxe() < o2.getTotalFaraTaxe())
					return -1;
				return 0;
			}
		});
	}
	
	void sortareFacturi(Vector <Factura> factura) {
		Collections.sort(factura, new Comparator<Factura>(){
			public int compare(Factura o1, Factura o2) {
				if(o1.getTotalFaraTaxe() > o2.getTotalFaraTaxe())
					return 1;
				if(o1.getTotalFaraTaxe() < o2.getTotalFaraTaxe())
					return -1;
				return 0;
			}
		});
	}
	
	void scriereMagazin(ArrayList <Magazin> magazin, String tip, BufferedWriter writeFile) {
		String str[] = citireTari();
		ArrayList <String> tari = new ArrayList<>();
		for(int i = 1; i < str.length; i++)
			tari.add(str[i]);
		Collections.sort(tari);
		for(int i = 0; i < tari.size(); i++)
			str[i + 1] = new String(tari.get(i));
		Magazin mag;
		Factura fact;
		Iterator <Magazin> iter;
		try {
			sortareMagazine(magazin);
			iter = magazin.iterator();			
			while(iter.hasNext()) {
				sortareFacturi(iter.next().factura);
			}
			switch(tip) {
				case "mini":
					writeFile.write("MiniMarket");
					writeFile.newLine();
					break;
				case "medium":
					writeFile.write("MediumMarket\n");
					writeFile.newLine();
					break;
				case "hyper":
					writeFile.write("HyperMarket\n");
					writeFile.newLine();
					break;
			}
			iter = magazin.iterator();
			while(iter.hasNext()) {
				mag = iter.next();
				writeFile.write(mag.nume);
				writeFile.newLine();
				writeFile.newLine();
				writeFile.write("Total" + " " + String.format("%.3f", mag.getTotalFaraTaxe()) + " " + String.format("%.3f", mag.getTotalCuTaxe()) + " " + String.format("%.3f", mag.getTotalCuTaxeScutite()));
				writeFile.newLine();
				writeFile.newLine();
				writeFile.write("Tara");
				writeFile.newLine();
				for(int i = 1; i < str.length; i++) {
					if(mag.getTotalTaraFaraTaxe(str[i]) == 0.0 && mag.getTotalTaraCuTaxe(str[i]) == 0.0 && mag.getTotalTaraCuTaxeScutite(str[i]) == 0.0)
						writeFile.write(str[i] + " " + 0);
					else
							writeFile.write(str[i] + " " + String.format("%.3f", mag.getTotalTaraFaraTaxe(str[i])) + " " + String.format("%.3f", mag.getTotalTaraCuTaxe(str[i])) + " " + String.format("%.3f", mag.getTotalTaraCuTaxeScutite(str[i])));
					writeFile.newLine();
				}
				Iterator <Factura> iterFact = mag.factura.iterator();
				while(iterFact.hasNext()) {
					writeFile.newLine();
					fact = iterFact.next();
					writeFile.write(fact.denumire);
					writeFile.newLine();
					writeFile.newLine();
					writeFile.write("Total " + String.format("%.3f", fact.getTotalFaraTaxe()) + " " + String.format("%.3f", fact.getTotalCuTaxe()));
					writeFile.newLine();
					writeFile.newLine();
					for(int i = 1; i < str.length; i++) {
						if(fact.getTotalTaraFaraTaxe(str[i]) == 0.0 && fact.getTotalTaraCuTaxe(str[i]) == 0)
							writeFile.write(str[i] + " " + 0);
						else
							writeFile.write(str[i] + " " + String.format("%.3f", fact.getTotalTaraFaraTaxe(str[i])) + " " + String.format("%.3f", fact.getTotalTaraCuTaxe(str[i])));
						writeFile.newLine();
					}
				}
				writeFile.newLine();
			}
			
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	void scriereFisier() {
		try {
			fos = new FileOutputStream(fout);
			writeFile = new BufferedWriter(new OutputStreamWriter(fos));
			scriereMagazin(miniMarket, "mini", writeFile);
			scriereMagazin(mediumMarket, "medium", writeFile);
			scriereMagazin(hyperMarket, "hyper", writeFile);
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		finally {
			if(writeFile != null)
				try {
					writeFile.close();
				} 
				catch (IOException e) {
					e.printStackTrace();
				}
		}
	}
}
