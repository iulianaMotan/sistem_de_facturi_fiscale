import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class MainInterfata {
	
	public static void main(String args[]) {
		 try {
	        UIManager.setLookAndFeel(
	            UIManager.getSystemLookAndFeelClassName());
	    } 
	    catch (UnsupportedLookAndFeelException e) {
	       System.out.println("Eroare!");
	    }
	    catch (ClassNotFoundException e) {
	    	System.out.println("Clasa negasita!");
	    }
	    catch (InstantiationException e) {
	    	System.out.println("Eroare de instantiere!");
	    }
	    catch (IllegalAccessException e) {
	    	System.out.println("Acces ilegal!");
	    }
		FereastraStart f = new FereastraStart("Sistem de Facturi Fiscale");
		f.creeazaFereastra();
		f.show();
	}
}
