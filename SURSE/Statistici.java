import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class Statistici {
	Gestiune gestiune = Gestiune.getInstance();
	ArrayList <String> tari = new ArrayList<>();
	ArrayList <String> categorii = new ArrayList<>();
	
	void gasesteTari() {
		RandomAccessFile readFile = null;
		String str = null;
		String vect[];
		try {
			readFile = new RandomAccessFile("src/produse.txt", "r");
			str = readFile.readLine();
			vect = str.split(" ");
			for(int i = 2; i < vect.length; i++)
				tari.add(vect[i]);
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
		finally {
			if(readFile != null) {
				try {
					readFile.close();
				}
				catch(IOException e){
					e.printStackTrace();
				}
			}
		}
	}
	
	void gasesteCategorii() {
		RandomAccessFile readFile = null;
		String str = null;
		String vect[];
		try {
			readFile = new RandomAccessFile("src/produse.txt", "r");
			str = readFile.readLine();
			while((str = readFile.readLine()) != null) {
				vect = str.split(" ");
				categorii.add(vect[1]);
			}
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
		finally {
			if(readFile != null) {
				try {
					readFile.close();
				}
				catch(IOException e){
					e.printStackTrace();
				}
			}
		}
	}
	
	String celeMaiMariVanzari() {
		Iterator <Magazin> iter = gestiune.magazine.iterator();
		Magazin magazin;
		double maxim = 0;
		Magazin magazinMaxim = null;
		while(iter.hasNext()) {
			magazin = iter.next();
			if(magazin.getTotalCuTaxe() > maxim) {
				maxim = magazin.getTotalCuTaxe();
				magazinMaxim = magazin;
			}
		}
		return String.format("%s %.2f %.2f %.2f", magazinMaxim.nume, magazinMaxim.getTotalFaraTaxe(), magazinMaxim.getTotalCuTaxe(), magazinMaxim.getTotalCuTaxeScutite());
	}
	
	String celeMaiMariVanzariTara() {
		gasesteTari();
		Iterator <Magazin> iter = gestiune.magazine.iterator();
		Magazin magazin;
		ArrayList <Double> maximTari = new ArrayList<>();
		int i;
		for(i = 0; i < tari.size(); i++)
			maximTari.add(0.0);
		Magazin magazinMaxim = null;
		while(iter.hasNext()) {
			magazin = iter.next();
			for(i = 0; i < tari.size(); i++) {
				if(magazin.getTotalTaraCuTaxe(tari.get(i)) < maximTari.get(i)) {
					break;
				}
				if(tari.size() - i <= 1) {
					for(i = 0; i < tari.size(); i++)
						maximTari.add(i, magazin.getTotalTaraCuTaxe(tari.get(i)));
					magazinMaxim = magazin;
				}
			}
		}
		return String.format("%s %.2f %.2f %.2f", magazinMaxim.nume, magazinMaxim.getTotalFaraTaxe(), magazinMaxim.getTotalCuTaxe(), magazinMaxim.getTotalCuTaxeScutite());
	}
	
	String celeMaiMariVanzariCategorie() {
		gasesteCategorii();
		Iterator <Magazin> iter = gestiune.magazine.iterator();
		Iterator <Factura> iterFact;
		Iterator <ProdusComandat> iterProd;
		Magazin magazin;
		Factura factura;
		ProdusComandat produsComandat;
		Produs produs;
		HashMap <String, Double> maximCategorii = new HashMap<>();
		for(int i = 0; i < categorii.size(); i++) {
			maximCategorii.put(categorii.get(i), 0.0);
		}
		HashMap <String, Double> totalCategorii = new HashMap<>();
		int i;
		Magazin magazinMaxim = null;
		while(iter.hasNext()) {
			for(i = 0; i < categorii.size(); i++) {
				totalCategorii.put(categorii.get(i), 0.0);
			}
			magazin = iter.next();
			iterFact = magazin.factura.iterator();
			while(iterFact.hasNext()) {
				factura = iterFact.next();
				iterProd = factura.produseComandate.iterator();
				while(iterProd.hasNext()) {
					produsComandat = iterProd.next();
					produs = produsComandat.getProdus();
					totalCategorii.put(produs.getCategorie(), totalCategorii.get(produs.getCategorie()) + produs.getPret() * produsComandat.getTaxa());
				}
			}
			for(i = 0; i < categorii.size(); i++) {
				if(totalCategorii.get(categorii.get(i)) < maximCategorii.get(categorii.get(i))) {
					break;
				}
			}
			if(categorii.size() - i <= 1) {
				magazinMaxim = magazin;
				for(i = 0; i < categorii.size(); i++)
					maximCategorii.put(categorii.get(i), totalCategorii.get(categorii.get(i)));
			}
		}
		return String.format("%s %.2f %.2f %.2f", magazinMaxim.nume, magazinMaxim.getTotalFaraTaxe(), magazinMaxim.getTotalCuTaxe(), magazinMaxim.getTotalCuTaxeScutite());
	}
	
	String ceaMaiMareFactura() {
		Iterator <Magazin> iter = gestiune.magazine.iterator();
		Iterator <Factura> iterFact;
		Magazin magazin;
		Factura factura, facturaMaxima = null;
		double maxim = 0;
		while(iter.hasNext()) {
			magazin = iter.next();
			iterFact = magazin.factura.iterator();
			while(iterFact.hasNext()) {
				factura = iterFact.next();
				if(factura.getTotalFaraTaxe() > maxim) {
					maxim = factura.getTotalFaraTaxe();
					facturaMaxima = factura;
				}
			}
		}
		return String.format("%s %.2f %.2f", facturaMaxima.denumire, facturaMaxima.getTotalFaraTaxe(), facturaMaxima.getTotalCuTaxe());
	}

}
