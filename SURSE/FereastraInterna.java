import java.awt.Dimension;

import javax.swing.JInternalFrame;

class FereastraInterna extends JInternalFrame {
	static int n = 0; // nr. de ferestre interne
	static final int x = 10, y = 10;
	public FereastraInterna (String str) {
		super (str,
			true , // resizable
			true , // closable
			true , // maximizable
			true );// iconifiable
		setLocation (x * n, y * n);
		setSize ( new Dimension (300 , 300) );
	}
}