import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Vector;

public class CreeazaFactura {
	Gestiune gestiune = Gestiune.getInstance();
	CreeazaProdusComandat obiectProdusComandat = new CreeazaProdusComandat();
	Factura factura;
	ArrayList <Factura> facturi = new ArrayList<>();
	RandomAccessFile file = null;
	Vector <ProdusComandat> deAdaugat;
	String str, denumire;
	int contor = 0;
	void creeazaFactura() {
		obiectProdusComandat.creeazaProdusComandat();
		try {
			file = new RandomAccessFile("src/facturi.txt", "r");
			str = file.readLine();
			do {
				if(str.startsWith("Factura")) {
					denumire = new String(str);
					str = file.readLine();
					str = file.readLine();
					while(!str.isEmpty()) {
						contor++;
						str = file.readLine();
						if(str == null)
							break;
					}
					deAdaugat = new Vector<>();
					for(int i = 0;i < contor; i++) 
						deAdaugat.add(obiectProdusComandat.produseComandate.get(i));
					factura = new Factura(denumire, deAdaugat);
					facturi.add(factura);
					obiectProdusComandat.produseComandate.removeAll(deAdaugat);
					contor = 0;
				}
				str = file.readLine();
			}
			while(str != null);	
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
		finally {
			if(file != null) {
				try {
					file.close();
				}
				catch(IOException e){
					e.printStackTrace();
				}
			}
		}
	}
}
