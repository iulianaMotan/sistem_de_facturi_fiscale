import java.util.ArrayList;

public class Main {
	
	public static void main(String[] args) {
		CompleteazaGestiune obiectGestiune= new CompleteazaGestiune();
		obiectGestiune.citeste();
		obiectGestiune.completeaza();
		CreeazaProdus obiectProdus = new CreeazaProdus();
		obiectProdus.creeaza();
		CreeazaMagazin magazin = new CreeazaMagazin();
		magazin.creeazaMagazin();
		ScriereOut obiectOut = new ScriereOut();
		obiectOut.atribuireMagazine();
		obiectOut.scriereFisier();
	}
	
}
