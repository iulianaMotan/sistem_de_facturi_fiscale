
public class ProdusComandat {

	private Produs produs;
	private double taxa;
	private int cantitate;
	
	public ProdusComandat(Produs produs, double taxa, int cantitate) {
		this.produs = new Produs(produs.getDenumire(), produs.getCategorie(), produs.getTaraOrigine(), produs.getPret());
		this.taxa = taxa;
		this.cantitate = cantitate;
	}
	public ProdusComandat() {
		this(null, 0, 0);
	}
	
	void setProdus(Produs produs) {
		this.produs.setDenumire(produs.getDenumire());
		this.produs.setCategorie(produs.getCategorie());
		this.produs.setTaraOrigine(produs.getTaraOrigine());
		this.produs.setPret(produs.getPret());
	}
	
	Produs getProdus() {
		return this.produs;
	}
	
	void setTaxa(double taxa) {
		this.taxa = taxa;
	}
	
	double getTaxa() {
		return this.taxa;
	}
	
	void setCantitate(int cantitate) {
		this.cantitate = cantitate;
	}
	
	int getCantitate() {
		return this.cantitate;
	}
	
	public String toString() {
		return produs.toString() + " " + taxa + " " + cantitate; 
	}
	
}
