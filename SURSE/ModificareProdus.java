import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;

public class ModificareProdus {
	RandomAccessFile readFile = null;
	BufferedWriter writeFile = null;
	ArrayList <String> tari = new ArrayList<>();
	
	Gestiune gestiune = Gestiune.getInstance();
	
	String[] citireTari(){
		String str = null;
		try {
			readFile = new RandomAccessFile("src/produse.txt", "r");
			str = readFile.readLine();
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
		finally {
			if(readFile != null) {
				try {
					readFile.close();
				}
				catch(IOException e){
					e.printStackTrace();
				}
			}
		}
		return str.split(" ");
	}
	
	int adaugareProdus(String denumire, String categorie, ArrayList <String> pret) {
		String line = "";
		try {
			line = line + denumire + " " + categorie;
			for(int i = 0; i < pret.size(); i++)
				line = line + " " + pret.get(i);
			try {
				readFile = new RandomAccessFile("src/produse.txt", "r");
				String str = readFile.readLine();
				while(str != null) {
					if(str.equals(line))
						return -1;
					str = readFile.readLine();
				}
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			}
			finally {
				if(readFile != null) {
					try {
						readFile.close();
					}
					catch(IOException e){
						e.printStackTrace();
					}
				}
			}
			writeFile = new BufferedWriter(new FileWriter("src/produse.txt", true));
			writeFile.newLine();
			writeFile.append(line);
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			if(writeFile != null) {
				try {
					writeFile.close();
				}
				catch(IOException e){
					e.printStackTrace();
				}
			}
		}
		return 0;
	}
	
	int stergereProdus(String denumire, String categorie) {
		File inputFile = new File("src/produse.txt");
		File outputFile = new File("src/produse.txt");
		BufferedReader readFile = null;
		BufferedWriter writeFile = null;
		try {
			readFile = new BufferedReader(new FileReader(inputFile));
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		String line = "", str;
		ArrayList <String> toWrite = new ArrayList<>();
		int success = -1;
		try {
			line = line + denumire + " " + categorie;
			while((str = readFile.readLine()) != null) {
				if(str.contains(line)) {
					success = 0;
					continue;
				}
				toWrite.add(str);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			if(readFile != null) {
				try {
					readFile.close();
				}
				catch(IOException e){
					e.printStackTrace();
				}
				
			}
		}
		try {
			writeFile = new BufferedWriter(new FileWriter(outputFile));
			for(int i = 0; i < toWrite.size(); i++) {
				writeFile.write(toWrite.get(i));
				if(toWrite.size() - i != 1)
					writeFile.newLine();
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		finally {
			if(writeFile != null) {
				try {
					writeFile.close();
				}
				catch(IOException e){
					e.printStackTrace();
				}
				
			}
		}
		return success;
	}
}
