import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Iterator;;

public class CreeazaProdusComandat {
	Gestiune gestiune = Gestiune.getInstance();
	ArrayList <ProdusComandat> produseComandate = new ArrayList<>();
	RandomAccessFile file = null;
	String str;
	double taxa;
	int cantitate;
	String[] vect;
	Produs produs;
	ProdusComandat produsComandat;
	void creeazaProdusComandat() {
		try {
			file = new RandomAccessFile("src/facturi.txt", "r");
			str = file.readLine();
			do {
				while(str.startsWith("Magazin") || str.startsWith("Factura") || str.startsWith("Denumire") || str.isEmpty())
						str = file.readLine();
				vect = str.split(" ");
				Iterator <Produs> iter = gestiune.produse.iterator();
				while(iter.hasNext()) {
					produs = iter.next();
					if(produs.getDenumire().equals(vect[0]) && produs.getTaraOrigine().equals(vect[1])) {
						taxa = gestiune.taxe.get(vect[1]).get(produs.getCategorie());
						cantitate = Integer.parseInt(vect[2]);
						produsComandat = new ProdusComandat(produs, taxa, cantitate);
						break;
					}
				}
				produseComandate.add(produsComandat);
				str = file.readLine();
			}
			while(str != null);
				
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
		finally {
			if(file != null) {
				try {
					file.close();
				}
				catch(IOException e){
					e.printStackTrace();
				}
			}
		}
	}
}
