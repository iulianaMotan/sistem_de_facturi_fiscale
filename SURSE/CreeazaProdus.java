import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Vector;

public class CreeazaProdus {
	
	Gestiune gestiune = Gestiune.getInstance();
	RandomAccessFile file = null;
	String str;
	String[] tari, vect;
	Produs produs;
	ArrayList <Produs> vectProduse = new ArrayList<>(); 
	void creeaza() {
		try {
			file = new RandomAccessFile("src/produse.txt", "r");
			str = file.readLine();
			tari = str.split(" ");
			str = file.readLine();
			do {
				vect = str.split(" ");
				for(int i = 2; i < vect.length; i++) {
					produs = new Produs(vect[0], vect[1], tari[i], Double.parseDouble(vect[i]));
					vectProduse.add(produs);
				}
				str = file.readLine();
			}
			while(str != null);
			gestiune.produse = vectProduse;	
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
		finally {
			if(file != null) {
				try {
					file.close();
				}
				catch(IOException e){
					e.printStackTrace();
				}
			}
		}
	}
	
	void ordoneazaAlfabetic() {
		Collections.sort(gestiune.produse, new Comparator<Produs>() {
			public int compare(Produs arg0, Produs arg1) {
				return arg0.getDenumire().compareTo(arg1.getDenumire());
			}
		});
	}
	
	void ordoneazaDupaTara() {
		Collections.sort(gestiune.produse, new Comparator<Produs>() {
			public int compare(Produs arg0, Produs arg1) {
				return arg0.getTaraOrigine().compareTo(arg1.getTaraOrigine());
			}
		});
	}
	
	String creeazaString(){
		Iterator <Produs> iter = gestiune.produse.iterator();
		String str = "";
		while(iter.hasNext()) {
			str = str + iter.next().toString() + "\n";
		}
		return str;
	}
}
